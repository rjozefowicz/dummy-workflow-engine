package com.r6lab.workflow.handler.reduction;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.r6lab.workflow.handler.WorkflowHandler;
import com.r6lab.workflow.handler.WorkflowHandlerConfiguration;

public class CountWorkflowHandler implements WorkflowHandler {

    public JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration) {
        if (object instanceof JsonArray) {
            JsonObject result = new JsonObject();
            result.addProperty("result", ((JsonArray) object).size());
            return result;
        } else {
            throw new IllegalStateException("Only arrays can be reduced");
        }
    }

}
