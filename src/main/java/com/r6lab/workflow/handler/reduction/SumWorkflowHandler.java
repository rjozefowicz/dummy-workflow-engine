package com.r6lab.workflow.handler.reduction;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.r6lab.workflow.handler.WorkflowHandler;
import com.r6lab.workflow.handler.WorkflowHandlerConfiguration;

import java.math.BigDecimal;

/**
 * Fuck JsonSchema, bruteforce that shit...
 */
public class SumWorkflowHandler implements WorkflowHandler {

    public JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration) {

        JsonObject result = new JsonObject();

        if (object instanceof JsonArray) {
            ((JsonArray) object).iterator().forEachRemaining(jsonElement -> {
                jsonElement.getAsJsonObject().entrySet().forEach(e -> {
                    String valueToReduce = e.getValue().getAsString();
                    if (isNumber(valueToReduce)) {
                        BigDecimal numericValueToReduce = new BigDecimal(valueToReduce);
                        JsonElement reducedValue = result.get(e.getKey());
                        if (reducedValue != null) {
                            result.addProperty(e.getKey(), reducedValue.getAsBigDecimal().add(numericValueToReduce));
                        } else {
                            result.addProperty(e.getKey(), numericValueToReduce);
                        }
                    } else {
                        JsonElement reducedValue = result.get(e.getKey());
                        if (reducedValue != null) {
                            result.addProperty(e.getKey(), reducedValue.getAsString() + valueToReduce);
                        } else {
                            result.addProperty(e.getKey(), valueToReduce);
                        }
                    }
                });
            });
        } else {
            throw new IllegalStateException("Only arrays can be reduced");
        }

        return result;
    }

}
