package com.r6lab.workflow.handler.reduction;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.r6lab.workflow.handler.WorkflowHandler;
import com.r6lab.workflow.handler.WorkflowHandlerConfiguration;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;

public class AverageWorkflowHandler implements WorkflowHandler {

    public JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration) {
        JsonObject result = new JsonObject();

        if (object instanceof JsonArray) {
            int counter = 0;
            Iterator<JsonElement> jsonElements = ((JsonArray) object).iterator();
            while (jsonElements.hasNext()) {
                JsonElement jsonElement = jsonElements.next();
                Iterator<Map.Entry<String, JsonElement>> properties = jsonElement.getAsJsonObject().entrySet().iterator();
                while (properties.hasNext()) {
                    Map.Entry<String, JsonElement> property = properties.next();
                    String valueToReduce = property.getValue().getAsString();
                    if (isNumber(valueToReduce)) {
                        BigDecimal numericValueToReduce = new BigDecimal(valueToReduce);
                        JsonElement reducedValue = result.get(property.getKey());
                        if (reducedValue != null) {
                            BigDecimal sum = reducedValue.getAsBigDecimal().add(numericValueToReduce);
                            result.addProperty(property.getKey(), sum);
                            counter++;
                        } else {
                            result.addProperty(property.getKey(), numericValueToReduce);
                            counter++;
                        }
                    } else {
                        // average on strings... it should throw an exception
                        result.addProperty(property.getKey(), "NaN");
                    }
                }
            }

            return calculateAverage(result, counter);

        } else {
            throw new IllegalStateException("Only arrays can be reduced");
        }
    }

    private JsonObject calculateAverage(JsonObject result, int finalCounter) {
        result.entrySet().forEach(e -> {
            String propertyValue = e.getValue().getAsString();
            if (isNumber(propertyValue)) {
                result.addProperty(e.getKey(), new BigDecimal(propertyValue).divide(new BigDecimal(finalCounter)));
            }
        });
        return result;
    }

}
