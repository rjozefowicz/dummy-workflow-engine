package com.r6lab.workflow.handler;

import com.google.gson.JsonObject;

public final class WorkflowHandlerConfiguration {

    private final JsonObject configuration;

    public WorkflowHandlerConfiguration(JsonObject configuration) {
        this.configuration = configuration;
    }

    public JsonObject getConfiguration() {
        return configuration;
    }

}
