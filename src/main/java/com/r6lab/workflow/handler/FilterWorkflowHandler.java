package com.r6lab.workflow.handler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

/**
 * e.g. expression:
 * value > 25 && value < 50
 * <p>
 *     should follow https://commons.apache.org/proper/commons-jexl/reference/syntax.html
 * </p>
 */
public class FilterWorkflowHandler implements WorkflowHandler {

    private final JexlEngine jexlEngine = new JexlBuilder().silent(true).strict(true).create();

    public JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration) {

        if (object instanceof JsonArray) {
            JsonArray result = new JsonArray();

            String expression = configuration.getConfiguration().get("expression").getAsString();
            JexlExpression jexlExpression = jexlEngine.createExpression(expression);

            ((JsonArray) object).iterator().forEachRemaining(jsonElement -> {
                MapContext jexlContext = new MapContext();
                jsonElement.getAsJsonObject().entrySet().forEach(properties -> {
                    if (expression.contains(properties.getKey())) {
                        if (isNumber(properties.getValue().getAsString())) {
                            jexlContext.set("$" + properties.getKey(), properties.getValue().getAsDouble());
                        } else {
                            jexlContext.set("$" + properties.getKey(), properties.getValue());
                        }
                    }
                });
                Object evaluatedExpressionResult = jexlExpression.evaluate(jexlContext);
                if (evaluatedExpressionResult != null && evaluatedExpressionResult instanceof Boolean) {
                    if ((boolean) evaluatedExpressionResult) {
                        result.add(jsonElement);
                    }
                } else {
                    throw new IllegalArgumentException("Expression should return boolean value");
                }
            });

            return result;

        } else {
            throw new IllegalStateException("Only arrays could be filtered");
        }
    }

}
