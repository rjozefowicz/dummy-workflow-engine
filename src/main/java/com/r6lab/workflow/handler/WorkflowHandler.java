package com.r6lab.workflow.handler;

import com.google.gson.JsonElement;

import java.math.BigDecimal;

/**
 * <p>All handlers accept JsonElement as input parameter and return new one</p>
 * <p></p>
 * <p>In general it should use JsonPath to extract data. In that case there are absolutely no limitations, no flat structure anymore...
 * and still it is not rocket science to do it like that
 * </p>
 */
public interface WorkflowHandler {
    JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration);

    /**
     * for now this project just recognize two types of values: numeric and string (this still should be enough...)
     * @param value
     * @return
     */
    default boolean isNumber(String value) {
        try {
            new BigDecimal(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
