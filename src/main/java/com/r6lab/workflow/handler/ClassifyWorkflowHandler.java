package com.r6lab.workflow.handler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * e.g. config
 * {
 * classification: [
 * {
 * property: "value",
 * default: "C",
 * values: [
 * {
 * expression: "value > 2 && value < 5",
 * result: "A"
 * },
 * {
 * expression: "value >= 5 && value < 10",
 * result: "B"
 * }
 * ]
 * },
 * {
 * property: "value1",
 * default: "WHO CARES",
 * values: [
 * {
 * expression: "value1 == 'A'",
 * result: "ANITA"
 * }
 * ]
 * }
 * ]
 * }
 */
public class ClassifyWorkflowHandler implements WorkflowHandler {

    private final JexlEngine jexlEngine = new JexlBuilder().silent(true).strict(true).create();

    public JsonElement handle(JsonElement object, WorkflowHandlerConfiguration configuration) {
        JsonArray classification = configuration.getConfiguration().getAsJsonArray("classification");
        if (object instanceof JsonObject) {
            return classify(object.getAsJsonObject(), classification);
        } else if (object instanceof JsonArray) {
            JsonArray result = new JsonArray();
            ((JsonArray) object).iterator().forEachRemaining(jsonElement -> {
                result.add(classify(jsonElement.getAsJsonObject(), classification));
            });
            return result;
        } else {
            throw new IllegalStateException("Unknown object to be classified");
        }
    }

    private JsonObject classify(JsonObject jsonObject, JsonArray classification) {

        JsonObject mappedJsonObject = new JsonObject();

        Map<String, ClassificationContainer> expressions = new HashMap<>();
        classification.iterator().forEachRemaining(jsonElement -> {
            String property = jsonElement.getAsJsonObject().get("property").getAsString();
            String defaultValue = jsonElement.getAsJsonObject().get("default").getAsString();
            JsonArray values = jsonElement.getAsJsonObject().getAsJsonArray("values");
            expressions.put(property, new ClassificationContainer(defaultValue, values));
        });

        Set<Map.Entry<String, JsonElement>> entries = jsonObject.entrySet();
        for (Map.Entry<String, JsonElement> entry : entries) {
            String key = entry.getKey();
            ClassificationContainer classificationContainer = expressions.get(key);
            if (classificationContainer != null) {
                Iterator<JsonElement> iterator = classificationContainer.getExpressions().iterator();
                while (iterator.hasNext()) {
                    // iterating through expressions
                    JsonElement jsonElement = iterator.next();
                    JsonObject value = jsonElement.getAsJsonObject();
                    String expression = value.get("expression").getAsString();
                    String result = value.get("result").getAsString();
                    MapContext jexlContext = new MapContext();

                    jsonObject.entrySet().forEach(en -> {
                        if (expression.contains(en.getKey())) {
                            if (isNumber(en.getValue().getAsString())) {
                                jexlContext.set(en.getKey(), en.getValue().getAsDouble());
                            } else {
                                jexlContext.set(en.getKey(), en.getValue().getAsString());
                            }
                        }
                    });

                    JexlExpression jexlExpression = jexlEngine.createExpression(expression);
                    Object evaluatedExpression = jexlExpression.evaluate(jexlContext);
                    if (evaluatedExpression != null && evaluatedExpression instanceof Boolean && (boolean) evaluatedExpression) {
                        mappedJsonObject.addProperty(entry.getKey(), result);
                        break;
                    }
                }
            } else {
                System.out.println("keep current value");
            }
        }
        return mappedJsonObject;
    }

    private class ClassificationContainer {
        private final String defaultValue;
        private final JsonArray expressions;

        public ClassificationContainer(String defaultValue, JsonArray expressions) {
            this.defaultValue = defaultValue;
            this.expressions = expressions;
        }

        public JsonArray getExpressions() {
            return expressions;
        }

        public String getDefaultValue() {
            return defaultValue;
        }
    }

}
