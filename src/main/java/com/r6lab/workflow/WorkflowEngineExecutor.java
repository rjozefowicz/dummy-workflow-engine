package com.r6lab.workflow;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.r6lab.workflow.handler.ClassifyWorkflowHandler;
import com.r6lab.workflow.handler.FilterWorkflowHandler;
import com.r6lab.workflow.handler.WorkflowHandler;
import com.r6lab.workflow.handler.WorkflowHandlerConfiguration;
import com.r6lab.workflow.handler.reduction.AverageWorkflowHandler;
import com.r6lab.workflow.handler.reduction.CountWorkflowHandler;
import com.r6lab.workflow.handler.reduction.MaxWorkflowHandler;
import com.r6lab.workflow.handler.reduction.MinWorkflowHandler;
import com.r6lab.workflow.handler.reduction.SumWorkflowHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this code in general is fckn disaster. Uncle bob would kill himself seeing that...
 * but still it's extremely lightweight
 */
public class WorkflowEngineExecutor {

    private final Map<WorkflowHandler, WorkflowHandlerConfiguration> handlers = new LinkedHashMap<>();

    public void addHandler(WorkflowHandler workflowHandler, WorkflowHandlerConfiguration configuration) {
        handlers.put(workflowHandler, configuration);
    }

    public JsonElement execute(JsonElement jsonElement) {

        JsonElement result = jsonElement;

        for (Map.Entry<WorkflowHandler, WorkflowHandlerConfiguration> entry : handlers.entrySet()) {
            result = entry.getKey().handle(result, entry.getValue());
        }

        return result;
    }

    public void printWorkflowExecutors() {
        System.out.println("Workflow handlers:");
        handlers.keySet().forEach(workflowHandler -> System.out.println(workflowHandler.getClass().getSimpleName()));
    }

    public static void main(String[] args) {

        WorkflowEngineExecutor workflowEngineExecutor = new WorkflowEngineExecutor();

        // empty run
        System.out.println("Empty workflow");
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Result: " + workflowEngineExecutor.execute(new JsonObject()).toString());
        System.out.println();

        // basic reduction - sum
        JsonObject configuration = new JsonObject(); //no need to configure reduction
        JsonObject schema = new JsonObject();
        schema.add("properties", new JsonObject());

        JsonArray testData = new JsonArray();
        JsonObject arrayElement1 = new JsonObject();
        arrayElement1.addProperty("numericValue", 25);
        arrayElement1.addProperty("stringValue", "A");
        JsonObject arrayElement2 = new JsonObject();
        arrayElement2.addProperty("numericValue", 35);
        arrayElement2.addProperty("stringValue", "B");
        JsonObject arrayElement3 = new JsonObject();
        arrayElement3.addProperty("numericValue", 45);
        arrayElement3.addProperty("stringValue", "C");
        testData.add(arrayElement1);
        testData.add(arrayElement2);
        testData.add(arrayElement3);


        System.out.println("Test Data: " + testData.toString());
        workflowEngineExecutor.addHandler(new SumWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction - sum");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but count reduction
        workflowEngineExecutor = new WorkflowEngineExecutor();
        workflowEngineExecutor.addHandler(new CountWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction count");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but max reduction
        workflowEngineExecutor = new WorkflowEngineExecutor();
        workflowEngineExecutor.addHandler(new MaxWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction max");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but min reduction
        workflowEngineExecutor = new WorkflowEngineExecutor();
        workflowEngineExecutor.addHandler(new MinWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction min");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but average reduction
        workflowEngineExecutor = new WorkflowEngineExecutor();
        workflowEngineExecutor.addHandler(new AverageWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction average");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but let's filter them than calculate average
        workflowEngineExecutor = new WorkflowEngineExecutor();
        configuration = new JsonObject();
        configuration.addProperty("expression", "$numericValue > 25");
        workflowEngineExecutor.addHandler(new FilterWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.addHandler(new AverageWorkflowHandler(), new WorkflowHandlerConfiguration(new JsonObject()));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction average");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but let's filter them than get min
        workflowEngineExecutor = new WorkflowEngineExecutor();
        configuration = new JsonObject();
        configuration.addProperty("expression", "$numericValue > 25");
        workflowEngineExecutor.addHandler(new FilterWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.addHandler(new MinWorkflowHandler(), new WorkflowHandlerConfiguration(new JsonObject()));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction average");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // same data but let's filter them than get sum
        workflowEngineExecutor = new WorkflowEngineExecutor();
        configuration = new JsonObject();
        configuration.addProperty("expression", "$numericValue > 25");
        workflowEngineExecutor.addHandler(new FilterWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.addHandler(new SumWorkflowHandler(), new WorkflowHandlerConfiguration(new JsonObject()));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Basic reduction average");
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();


        // more fun: let's filter out data outside (20,60), than evaluate average, than map it to: (20, 40) -> X, (40, 60) -> Y
        workflowEngineExecutor = new WorkflowEngineExecutor();
        testData = new JsonArray();
        arrayElement1 = new JsonObject();
        arrayElement1.addProperty("numericValue", 15);
        arrayElement1.addProperty("stringValue", "A");
        arrayElement2 = new JsonObject();
        arrayElement2.addProperty("numericValue", 25);
        arrayElement2.addProperty("stringValue", "B");
        arrayElement3 = new JsonObject();
        arrayElement3.addProperty("numericValue", 35);
        arrayElement3.addProperty("stringValue", "C");
        JsonObject arrayElement4 = new JsonObject();
        arrayElement4.addProperty("numericValue", 45);
        arrayElement4.addProperty("stringValue", "C");
        testData.add(arrayElement1);
        testData.add(arrayElement2);
        testData.add(arrayElement3);
        testData.add(arrayElement4);


        // real fun, more complex workflow: filter out unneeded data, calculate average, classify data
        JsonObject configurationConfiguration = new JsonObject();
        JsonArray classification = new JsonArray();
        JsonObject classificationA = new JsonObject();
        classificationA.addProperty("default", "X");
        classificationA.addProperty("property", "numericValue");
        JsonArray valuesA = new JsonArray();
        JsonObject valueA = new JsonObject();
        valueA.addProperty("expression", "numericValue == 30");
        valueA.addProperty("result", "VALUE 30");
        JsonObject valueB = new JsonObject();
        valueB.addProperty("expression", "numericValue == 40");
        valueB.addProperty("result", "VALUE 40");
        valuesA.add(valueA);
        valuesA.add(valueB);
        classificationA.add("values", valuesA);
        classification.add(classificationA);
        configurationConfiguration.add("classification", classification);


        System.out.println("More complex workflow: filter out unneeded data, calculate average, classify data");
        configuration = new JsonObject();
        configuration.addProperty("expression", "$numericValue > 20 && $numericValue < 40");
        workflowEngineExecutor.addHandler(new FilterWorkflowHandler(), new WorkflowHandlerConfiguration(configuration));
        workflowEngineExecutor.addHandler(new AverageWorkflowHandler(), new WorkflowHandlerConfiguration(new JsonObject()));
        workflowEngineExecutor.addHandler(new ClassifyWorkflowHandler(), new WorkflowHandlerConfiguration(configurationConfiguration));
        workflowEngineExecutor.printWorkflowExecutors();
        System.out.println("Test Data: " + testData.toString());
        System.out.println("FilterWorkflowHandler configuration data: " + configuration.toString());
        System.out.println("ClassifyWorkflowHandler configuration data: " + configurationConfiguration.toString());
        System.out.println("Result: " + workflowEngineExecutor.execute(testData).toString());
        System.out.println();
    }


}
